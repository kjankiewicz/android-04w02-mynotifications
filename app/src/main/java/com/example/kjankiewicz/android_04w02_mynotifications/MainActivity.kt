package com.example.kjankiewicz.android_04w02_mynotifications

import android.app.Activity
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import android.view.*
import android.widget.Button
import android.widget.RemoteViews
import android.widget.Toast


class MainActivity : Activity() {
    private var mManager: NotificationManager? = null

    private fun notificationManager(): NotificationManager? {
        if (mManager == null) {
            mManager = getSystemService(
                    Context.NOTIFICATION_SERVICE) as NotificationManager
        }
        return mManager
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var button = findViewById<Button>(R.id.first_notification_button)
        button.setOnClickListener {

            val notBuilder: NotificationCompat.Builder

            val bm = BitmapFactory.decodeResource(
                    resources,
                    R.drawable.oak)

            // Tworzymy intencje dla akcji pozwiązanej z powiadomieniem
            val notificationIntent = Intent(applicationContext,
                    NotificationActivity::class.java)

            val pendingIntent = PendingIntent.getActivity(
                    applicationContext, 0,
                    notificationIntent,
                    PendingIntent.FLAG_ONE_SHOT)

            notBuilder = NotificationCompat.Builder(
                    this, DEFAULT_CHANNEL_ID)
                    .setTicker("Zostałeś odkryty!")
                    .setLargeIcon(bm)
                    .setSmallIcon(R.drawable.ic_notification)
                    .setContentTitle("Odkryty!")
                    .setContentText("Zostałeś odkryty!")
                    .setAutoCancel(false)
                    .setContentIntent(pendingIntent)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val name = getString(R.string.default_channel)
                val description = getString(R.string.default_channel_description)
                val importance = NotificationManager.IMPORTANCE_DEFAULT
                val channel = NotificationChannel(DEFAULT_CHANNEL_ID,
                        name, importance)
                channel.description = description
                notificationManager()?.createNotificationChannel(channel)
            }

            val notificationManager = NotificationManagerCompat.from(this)

            notificationManager.notify(MY_NOTIFICATION_ID, notBuilder.build())
        }


        button = findViewById(R.id.custom_notification_button)
        button.setOnClickListener {

            val notBuilder: NotificationCompat.Builder

            // Tworzymy intencje dla akcji pozwiązanej z powiadomieniem
            val notificationIntent = Intent(applicationContext,
                    NotificationActivity::class.java)

            val pendingIntent = PendingIntent.getActivity(
                    applicationContext, 0,
                    notificationIntent,
                    PendingIntent.FLAG_ONE_SHOT)

            val bm = BitmapFactory.decodeResource(
                    resources,
                    R.drawable.oak)

            val contentView = RemoteViews(
                    applicationContext.packageName,
                    R.layout.custom_notification)

            notBuilder = NotificationCompat.Builder(
                    applicationContext, DEFAULT_CHANNEL_ID)
                    .setTicker("Zostałeś odkryty!")
                    .setSmallIcon(R.drawable.ic_notification)
                    .setContentTitle("Odkryty!")
                    .setAutoCancel(false)
                    .setLargeIcon(bm)
                    // uzupełniamy budowę powiadomienia o wywołanie akcji
                    .setContentIntent(pendingIntent)
                    .setPriority(
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                                NotificationManager.IMPORTANCE_HIGH
                            else
                                NotificationCompat.PRIORITY_HIGH)
                    //.setProgress(100,30,false)
                    .setContent(contentView)

            val notificationManager = getSystemService(
                    Context.NOTIFICATION_SERVICE) as NotificationManager

            notificationManager.notify(
                    MY_SECOND_NOTIFICATION_ID,
                    notBuilder.build())
        }

        button = findViewById(R.id.simple_toast_button)
        button.setOnClickListener {
            val context = applicationContext
            val text = "Zostałeś odkryty!"
            val duration = Toast.LENGTH_SHORT

            val toast = Toast.makeText(context, text, duration)
            toast.show()
        }

        button = findViewById(R.id.custom_toast_button)
        button.setOnClickListener {
            val inflater = layoutInflater
            val customToastLayout = inflater.inflate(R.layout.custom_notification,
                    findViewById(R.id.custom_layout))

            val toast = Toast(applicationContext)
            toast.duration = Toast.LENGTH_LONG
            toast.view = customToastLayout
            toast.setGravity(Gravity.BOTTOM or Gravity.CENTER, 0, 0)
            toast.show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId
        return id == R.id.action_settings || super.onOptionsItemSelected(item)
    }

    companion object {
        const val MY_NOTIFICATION_ID = 1
        const val MY_SECOND_NOTIFICATION_ID = 2
        const val DEFAULT_CHANNEL_ID = "com.example.kjankiewicz.android_04w02_mynotifications.DEFAULT_CHANNEL"
    }
}
