package com.example.kjankiewicz.android_04w02_mynotifications

import android.app.Activity
import android.app.NotificationManager
import android.content.Context
import android.os.Bundle
import android.support.v4.app.NotificationManagerCompat
import android.view.Menu
import android.view.MenuItem

class NotificationActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification)
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.notification, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId
        return id == R.id.action_settings || super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        val notificationManager =
                NotificationManagerCompat.from(this)
        notificationManager.cancel(MainActivity.MY_NOTIFICATION_ID)
        super.onDestroy()
    }
}
